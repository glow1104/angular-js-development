var gulp = require("gulp"),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass')
;

var paths = {
    css: "styles/"
};

var autoprefixerOptions = {
    browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};

gulp.task('sass', function () {
    return gulp.src(paths.css + "/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass({ errLogToConsole: true }).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(paths.css));
});

gulp.task('watch:sass', function () {
    return gulp.watch(paths.css + "/*.scss", ['sass'])
       .on('change', function (event) {
          console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
      });
});

gulp.task('watch', ['watch:sass']);

