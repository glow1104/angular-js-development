<!-- Strategy and design challenges created by the internet of things -->
<div class="ibm-columns">
    <div class="ibm-col-6-4">
        <p>
            The Internet of Things (IoT) vastly expands the reach and impact of information technology by bridging the physical and digital worlds. IDC predicts that by 2020, the IoT will consist of more than 29 billion connected devices.<sup>1</sup> And experts are estimating that the data from these devices will yield insights that drive economic value of more than <a href="http://www.ibm.com/internet-of-things/learn/library/what-is-iot/" class="ibm-light ibm-textcolor-{{data.theme.primaryColor}}" target="_blank"><span class="ibm-textcolor-{{data.theme.primaryColor}}">$11 trillion</span></a> by 2025.<sup>2</sup> Some experts still feel these numbers are too conservative.
        </p>
    </div>
</div>
<div class="ibm-band ibm-background-{{data.theme.primaryColor}} ibm-padding-top-30">
    <div class="ibm-columns">
        <div class="ibm-col-6-4">
            <div ng-ibm-video="" id="'QSIPNhOiMoE'" source="'https://www.youtube.com/watch?v=QSIPNhOiMoE'" imagesize="'medium'"></div>
        </div>
        <div class="ibm-col-6-2">
            <h3 class="video-title ibm-h3 ibm-textcolor-white-core">The Internet of Things...<br /> How does it work?</h3>
            <p class="video-description ibm-textcolor-white-core">
                The Internet of Things gives us access to the data from millions of devices. But how does it work, and what can we do with all that data? Find out in this animated tutorial from IBM's Think Academy. For more information on IBM and the Internet of Things, please visit: <a href="http://www.ibm.com/IoT" class="ibm-light ibm-textcolor-white-core" target="_blank">http://www.ibm.com/IoT</a>
            </p>
        </div>
    </div>
</div>
<div class="ibm-columns">
    <div class="ibm-col-6-4">
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">Strategy and Design Challenges Raised by IoT</h3>
        <p>
            Strategically, companies are expecting IoT to have a major impact on their future business. <a href="http://www.ibm.com/internet-of-things/learn/library/what-is-iot/" class="ibm-light ibm-textcolor-{{data.theme.primaryColor}}" target="_blank"><span class="ibm-textcolor-{{data.theme.primaryColor}}">91% of companies</span></a> surveyed by IBM’s Institute for Business Value said they believe IoT will reshape their organization’s brand identity.<sup>3</sup>
        </p>
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">Increased product and service complexity</h3>
        <p>
            From a strategy and design perspective, IoT is making companies wrestle with increased product and service complexity. Industries are struggling with the challenges that electronics manufacturers have been dealing with over the past decade or more—namely how to incorporate software and sensors into previously unconnected products.
        </p>
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">New pricing, partnership and business model options</h3>
        <p>
            In addition to complexity, IoT is also giving companies new options to consider for pricing, business models and partnerships. Products with new sensors, connectivity and compute power enable better ways of measuring product usage and satisfaction. This in turn leads to new “as-as-service” offerings where customers pay for usage or outcomes instead of simply owning a product.
        </p>
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">Increased product and service expectations from end-users</h3>
        <p>
            IoT is also introducing new challenges from a product and service design perspective. Not only is complexity increasing for the device maker and end user, but end-user expectations are increasing as all devices become “smarter” and more capable in terms of compute power and connectivity. Consumers are seeing how easy it is to connect IoT devices, access information and digitally perform tasks or transactions that previously required some sort of physical interaction. This is subtly reshaping users’ expectations of their connected devices and environments. For example, why should homeowners have to remember to set their thermostat when they are out of the house? Why can’t the thermostat figure out that there isn’t anyone in the house? Likewise, why doesn't their refrigerator recommend a recipe for dinner based on the ingredients you have available? Why can’t their HVAC system automatically contact their maintenance provider and schedule its seasonal inspections based on their maintenance contract terms and the condition of their HVAC system?
        </p>
        <figure class="ibm-col-6-4" ibm-img-zoom source="'styles/images/figure_1.png'" alt="'“Internet of Things, IoT, cognitive IoT, IoT smart”'" caption="'The reality of intelligence in every room in your home is not far away. This infographic shows estimated smart appliance presence in the home. Additionally, Gartner estimates that there may be up to 500 smart devices in the “typical” smart home by 2022.<sup>4</sup>'" alternate="true" responsive-class="ibm-downsize" ratio-class="ratio-459-448"></figure>
    </div>
</div>
<div class="ibm-columns">
    <div class="ibm-col-6-4">
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">Increased desire to see “return on (personal) data”</h3>
        <p>
            As consumers are increasingly bombarded with requests for their data from device makers and service providers, they are increasingly expecting something of value in return. While IoT data is growing twice as fast as social and computer-generated data, and will soon be the largest single source of data on the planet, nearly 90% of this data is never acted upon. There are many reasons for this: IoT data may be too big and expensive to store or move, it may need to stay out of the cloud for legal reasons or it may be too complex to combine, analyze and act on in real time. The IoT data volume is particularly challenging because it is pushing the limits of today’s programmable computing systems. These systems thrive on prescribed scenarios using predictable data format and flow, but this rigidity limits their usefulness in addressing the ambiguity and uncertainty of IoT. These systems are reaching a point where they cannot efficiently handle the sheer scale, diversity and complexity of IoT.
        </p>
        <h3 class="ibm-h3 sub-section ibm-textcolor-{{data.theme.primaryColor}} ibm-bold">IoT implications for computation (and cognition)</h3>
        <p>
            IBM has addressed this challenge by building cognitive computing capabilities from Watson into its portfolio of IoT solutions.
        </p>
        <div class="highlighted ibm-background-{{data.theme.primaryColor}} ibm-textcolor-white-core ibm-bold">
            <h3 class="ibm-h3 ibm-textcolor-white-core ibm-bold">What is cognitive computing?</h3>
            <p>
                Cognitive computing is defined as the ability to learn at scale, reason with purpose and interact with humans naturally. The ability to relate concepts and data taken from many different sources in a wide variety of formats, both structured and unstructured, and then to learn from the data to uncover relationships and meaning are key capabilities to unlocking the potential of the Internet of Things.
            </p>
        </div>
    </div>
</div>
<div class="ibm-columns">
    <div class="ibm-col-6-4">
        <p>
            Cognitive systems are ideal for the challenge of IoT data since they are not explicitly programmed. Cognitive systems learn from interactions with users and from experiences with their environment. This learning model enables cognitive systems to keep pace with IoT complexity and chaos. In addition, cognitive systems can be “trained” in their relevant domain so as to quickly offer useful insight. We are now at a point where we have enough digital training data and computing power to make cognitive systems functional and generate actionable insights right away. On top of this, these cognitive capabilities can now be embedded in connected devices, paired with device-specific insights and aggregated with intelligence in the cloud. Imagine the power of this—a single data set from a connected consumer electronics device is useful, but something tremendously more valuable is created when multiple data sets are combined with IoT device information. For a consumer electronics manufacturer, customer contact records from the call center are certainly useful, but when linked with warranty claims, product sensor data, weather data and millions of customer usage records from similar customer cohorts, truly valuable insights start to emerge.
        </p>
        <p>
            IBM cognitive IoT capabilities give businesses and individuals the ability to harness this kind of data, compare it with historical data sets and deep reservoirs of accumulated knowledge, find unexpected correlations and generate new insights to benefit business and society alike.
        </p>
        <div class="highlighted ibm-background-{{data.theme.primaryColor}} ibm-textcolor-white-core ibm-bold">
            <h3 class="ibm-h3 ibm-textcolor-white-core ibm-bold">IoT factoids</h3>
            <ul class="ibm-bold">
                <li>
                    Today’s nearly 13 billion connected devices will grow to over 29 billion by 2020
                </li>
                <li>
                    Data from IoT will yield insights that drive economic value of more than $11 trillion by 2025
                </li>
                <li>
                    B2B users will generate 70% of this value, mostly through operational efficiency gains from initiatives like smart factories and connected supply chains
                </li>
                <li>
                    82% of enterprise decision-makers say IoT is either strategic or transformative to their industry
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="ibm-columns">
    <div class="ibm-col-6-4">
        <p>
            These cognitive capabilities give companies a key to unlock the value of IoT, but that is the not the end of their IoT journey—in fact it is barely the beginning of it. There are important strategy and design decisions for companies to make all along the value chain as they incorporate cognitive and other new capabilities into their business. There are also some important behaviors and patterns to avoid, i.e. the Seven Deadly Sins of IoT Strategy and Design, and companies should take note of these as they harness the potential of IoT.
        </p>
    </div>
</div>
