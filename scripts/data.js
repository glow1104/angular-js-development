var _appData = {
    enableScrollableMenu: false,
    enableStickyTitleLink: true,
    disableStickySocialShare: true,
    enableCollapsibleToC: true,
    enableShareThisPage: true,

    siteUrl: "http://ibm.biz/processcloud",
    appTitle: "The internet of things: seven deadly sins of strategy and design",

    theme: {
        primaryColor: 'teal-50',
        cover: {
            textColor: 'white-core'
        }
    },

    share: {
        title: "The internet of things: seven deadly sins of strategy and design",
        description: "Missteps, or “deadly sins,” to avoid around the Internet of Things within the market today",

        email: {
            subject: "Avoid the Seven Deadly Sins of IoT with IBM",
            bodyTemplate: "Get an in-depth look at how IoT solutions can go awry when best practices, thought leadership and customer experience are ignored. IBM expertise guides your business in the journey form development to deployment. Here's an excerpt: \r\n\r\n \"In a new dynamic market like IoT-enabled devices, market participants are still defining best practices and standard procedures. As companies wrestle with how they compete in their IoT-enabled industry, it is useful to watch the steps and missteps of others as they build their IoT strategy and go-to-market approach.\" \r\n\r\n {{text}} Here's the link to the full paper: {{siteUrl}}"
        },

        linkedIn: {
            text: "Learn how to innovate and deploy IoT solutions with expertise, precision and best practices in mind:",
            fn: function(data, selectedText) {
                var decodedUrl = decodeURIComponent(data.siteUrl);
                var maxLength = 250;
                var textLength = maxLength - decodedUrl.length - 8;

                var text = selectedText != ''
                    ? "“" +selectedText.substring(0, textLength - 1) + "“"
                    : data.share.linkedIn.text;

                var url = "http://www.linkedin.com/shareArticle?mini=true"
                    + "&url=" + data.siteUrl
                    + "&title=" + data.share.title
                    + "&source=IBM"
                    + "&summary=" + encodeURIComponent(text + ' ' + decodedUrl);

                return url;
            }
        },

        twitter: {
            text: "Avoid these 7 Deadly Sins when developing IoT solutions for your cusatomers:",
            fn: function(data, selectedText) {
                var decodedUrl = decodeURIComponent(data.siteUrl);
                var baseUrl = "https://twitter.com/intent/tweet";
                var maxLength = 140;
                var maxUrlLength = 22;
                var textLength = maxLength - maxUrlLength;
                var text = selectedText != ''
                    ? "“" + selectedText.substring(0, textLength - 3) + "”"
                    :  data.share.twitter.text;

                return baseUrl + "?text=" + encodeURIComponent(text + ' ' + decodedUrl);
            }
        }
    },

    sections: [
        [
            { id: 'content1', title: 'Strategy and design challenges created by the internet of things', contentLayout: 4, headerLayout: 2 },
            { id: 'content2', title: 'Avoiding the seven deadly sins of IoT', contentLayout: 4, headerLayout: 2 },
            { id: 'content3', title: 'The First Sin: Greed', contentLayout: 2 },
            { id: 'content4', title: 'The Second Sin: Envy', contentLayout: 4, headerLayout: 2 },
            { id: 'content5', title: 'The Third Sin: Gluttony', contentLayout: 2 },
            { id: 'content6', title: 'The Fourth Sin: Wrath', contentLayout: 2 }
        ],
        [
            { id: 'content7', title: 'The Fifth Sin: Lust', contentLayout: 2 },
            { id: 'content8', title: 'The Sixth Sin: Pride', contentLayout: 2 },
            { id: 'content9', title: 'The Seventh Sin: Sloth', contentLayout: 2 },
            { id: 'content10', title: 'How IBM IoT Industry Expertise Can Help', contentLayout: 2 },
            { id: 'content11', title: 'What’s Next?', contentLayout: 2 },
            { id: 'content12', title: 'Next steps', excludeFromToc: true, fullColored: true, nonCollapsible: true, contentLayout: 3 },
            { id: 'content13', title: 'Sources', excludeFromToc: true, nonCollapsible: true, contentLayout: 3 }
        ]
    ],
    keyPoints: [
        { topic: 'Innovate for your customers', target: 'the-first-sin-greed' },
        { topic: 'Lead by differentiating', target: 'the-second-sin-envy' },
        { topic: 'Focus on value delivery', target: 'the-third-sin-gluttony' },
        { topic: 'Help customers understand the IoT', target: 'the-fourth-sin-wrath' },
        { topic: 'Deliver innovations with meaning', target: 'the-fifth-sin-lust' },
        { topic: 'Create usable solutions', target: 'the-sixth-sin-pride' },
        { topic: 'Be diligent and direct', target: 'the-seventh-sin-sloth' }
    ]
};